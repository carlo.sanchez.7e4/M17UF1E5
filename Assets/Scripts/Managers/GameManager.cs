using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Game Manager is NULL!");
            }

            return _instance;
        }


    }

    private void Awake()
    {

        if (_instance != null && _instance != this)
        {

            Destroy(this.gameObject);
        }

        _instance = this;

    }

    private GameObject player;
    private PlayerData PD;
    private int HP;
    private Vector3 playerStartingPos;
    private CameraFollow CF;

    public float maxHeight;
    public int kills;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        PD = player.GetComponent<PlayerData>();
        
        HP = PD.HP;
        playerStartingPos = player.transform.position;
        CF = Camera.main.GetComponent<CameraFollow>();
        maxHeight = CF.maxHeight;
        kills = PD.kills;
    }

    // Update is called once per frame
    void Update()
    {
        maxHeight = CF.maxHeight;
        kills = PD.kills;

        Vector3 stageDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0));
        if (player.transform.position.y <= stageDimensions.y)
        {
            LostHP();
            if (HP <= 0)
            {
                if (maxHeight > PlayerPrefs.GetFloat("MaxHeight"))
                {
                    PlayerPrefs.SetFloat("MaxHeight", maxHeight);
                }
                if (kills > PlayerPrefs.GetInt("Kills"))
                {
                    PlayerPrefs.SetInt("Kills", kills);
                }
                PlayerPrefs.Save();
                SceneManager.LoadScene("GameOverScreen");
            }
        }

        PD.HP = HP;
    }

    public void LostHP()
    {
        HP--;
        CF.followHeight = 0.5f;
        CF.camT.position = CF.startingPos;
        player.transform.position = playerStartingPos;
    }

    public void GainHP()
    {
        HP++;
    }
}
