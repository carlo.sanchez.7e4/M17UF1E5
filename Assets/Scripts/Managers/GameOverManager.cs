using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    private static GameOverManager _instance;

    public static GameOverManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Game Over Manager is NULL!");
            }

            return _instance;
        }


    }

    private void Awake()
    {

        if (_instance != null && _instance != this)
        {

            Destroy(this.gameObject);
        }

        _instance = this;
    }

        // Start is called before the first frame update
        void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TryAgain();
    }

    private void TryAgain()
    {
        if (Input.GetMouseButton(0))
        {
            SceneManager.LoadScene("GameScreen");
        } else if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
