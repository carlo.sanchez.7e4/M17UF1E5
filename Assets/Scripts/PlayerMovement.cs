using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float speed;
    private float jumpStrength;
    private bool onGround;
    private Animator playerAnimator;
    private Rigidbody2D playerRB;
    private PlayerData PD;
    private Vector2 screenBounds;
    private float objectWidth;

    // Start is called before the first frame update
    void Start()
    {
        PD = gameObject.GetComponent<PlayerData>();
        speed = PD.Speed;
        jumpStrength = PD.jumpStrength;
        playerAnimator = gameObject.GetComponent<Animator>();
        onGround = PD.onGround;
        playerRB = gameObject.GetComponent<Rigidbody2D>();

        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (!onGround)
        {
            playerAnimator.SetBool("Jumping", true);
        }
        else
        {
            playerAnimator.SetBool("Jumping", false);
        }
        onGround = PD.onGround;
        PlayerMove();

        Vector3 viewPos = transform.position;
        viewPos.x = Mathf.Clamp(viewPos.x, screenBounds.x * -1 + objectWidth, screenBounds.x - objectWidth);
        transform.position = viewPos;
    }

    private void PlayerMove()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            PlayerMoveLeft();
            playerAnimator.SetBool("Walking", true);
            if (Input.GetKey(KeyCode.Space) && onGround)
            {
                playerAnimator.SetBool("Walking", false);
                PlayerJump();
            }
        }
        else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            PlayerMoveRight();
            playerAnimator.SetBool("Walking", true);
            if (Input.GetKey(KeyCode.Space) && onGround)
            {
                playerAnimator.SetBool("Walking", false);
                PlayerJump();
            }
        }
        else if (Input.GetKey(KeyCode.Space) && onGround)
        {
            PlayerJumpStraight();
        }
        else
        {
            playerAnimator.SetBool("Walking", false);
        }
    }

    private void PlayerJumpStraight()
    {
        playerRB.AddForce(transform.up * (jumpStrength / 2));
    }

    private void PlayerJump()
    {
        playerRB.AddForce(transform.up * jumpStrength);
    }

    private void PlayerMoveRight()
    {
        if (PD.facingLeft)
        {
            Flip(gameObject);
            PD.facingLeft = false;
        }

        transform.position += Vector3.right * (speed * Time.deltaTime);
    }

    private void PlayerMoveLeft()
    {
        if (!PD.facingLeft)
        {
            Flip(gameObject);
            PD.facingLeft = true;
        }

        transform.position += Vector3.left * (speed * Time.deltaTime);
    }

    private void Flip(GameObject target)
    {
        target.transform.Rotate(0f, 180f, 0f);
    }
}
