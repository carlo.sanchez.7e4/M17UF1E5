using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextManager : MonoBehaviour
{
    public enum textType{
        HP,
        Height
    }
    public textType selector;
    private Text textOutput;
    private GameObject player;
    private PlayerData PD;
    private CameraFollow CF;
    public int HP;
    public float maxHeight;

    // Start is called before the first frame update
    void Start()
    {
        textOutput = GetComponent<Text>();
        player = GameObject.FindGameObjectWithTag("Player");
        PD = player.GetComponent<PlayerData>();
        CF = Camera.main.GetComponent<CameraFollow>();
        HP = PD.HP;
        maxHeight = CF.maxHeight;
    }

    // Update is called once per frame
    void Update()
    {
        HP = PD.HP;
        maxHeight = CF.maxHeight;

        switch (selector)
        {
            case textType.HP:
                textOutput.text = "Life/s:\t" + HP;
                break;
            case textType.Height:
                textOutput.text = "Max Height:\t" + maxHeight;
                break;
        }
    }
}
