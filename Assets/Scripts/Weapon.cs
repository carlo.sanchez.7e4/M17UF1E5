using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject firePoint;
    public GameObject bullet;
    public GameObject gun;
    public bool hasGun;

    private void Start()
    {
        hasGun = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X) && hasGun)
        {
            Fire();
        }
    }

    private void Fire()
    {
        Instantiate(bullet, firePoint.transform.position, firePoint.transform.rotation);
    }
}
