using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;
    public float maxHeight;
    public float followHeight;
    public Transform camT;
    public Vector3 startingPos;

    private void Start()
    {
        followHeight = 0.5f;
        camT = transform;
        startingPos = transform.position;
        maxHeight = followHeight;
    }

    void LateUpdate()
    {
        Vector3 position = transform.position;
        position.y = (target.position + offset).y;
        if (target.position.y >= followHeight) {
            transform.position = position;
            followHeight = target.position.y;
        }
        if (followHeight >= maxHeight)
        {
            maxHeight = followHeight;
        }
    }
}
