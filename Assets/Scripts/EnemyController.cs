using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private GameObject player;
    private bool still;
    public GameObject drop;

    [SerializeField] private float speed;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        still = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (still)
        {
            StartAttack();
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.localPosition, player.transform.position, Time.deltaTime * speed);
        }

        Vector3 stageDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0));
        if (transform.position.y <= stageDimensions.y)
        {
            Destroy(gameObject);
        }
    }

    private void StartAttack()
    {
        if (CompareHeights())
        {
            still = false;
        }
    }

    private bool CompareHeights()
    {
        if (player.transform.position.y >= transform.position.y) return true;
        else return false;
    }

    private void OnDestroy()
    {
        float random = UnityEngine.Random.Range(0f, 10f);
        Debug.Log(random);
        if (random >= 0 && random <= 3)
        {
            Instantiate(drop, transform.position, drop.transform.rotation);
        }
    }
}
